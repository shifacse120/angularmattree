import { FlatTreeControl } from "@angular/cdk/tree";
import { Component } from "@angular/core";
import {
  MatTreeFlatDataSource,
  MatTreeFlattener
} from "@angular/material/tree";

/**
 * Food data with nested structure.
 * Each node has a name and an optiona list of children.
 */
interface FoodNode {
  name: string;
  read: boolean;
  write: boolean;
  edit: boolean;
  delete: boolean;
  allow: boolean;
  children?: FoodNode[];
}

const TREE_DATA: FoodNode[] = [
  {
    name: "Fruit",
    read: false,
    write: true,
    edit: true,
    delete: false,
    allow: true,
    children: [
      {
        name: "Apple ",
        read: true,
        write: true,
        edit: true,
        delete: true,
        allow: true
      },
      {
        name: "Banana",
        read: true,
        write: true,
        edit: true,
        delete: true,
        allow: true
      },
      {
        name: "Fruit loops",
        read: true,
        write: true,
        edit: true,
        delete: true,
        allow: true
      }
    ]
  },
  {
    name: "Vegetables",
    read: true,
    write: true,
    edit: true,
    delete: true,
    allow: true,
    children: [
      {
        name: "Green",
        read: true,
        write: true,
        edit: true,
        delete: true,
        allow: true,
        children: [
          {
            name: "Broccoli",
            read: true,
            write: true,
            edit: false,
            delete: true,
            allow: true
          },
          {
            name: "Brussel sprouts",
            read: true,
            write: true,
            edit: false,
            delete: true,
            allow: true
          }
        ]
      },
      {
        name: "Orange",
        read: false,
        write: false,
        edit: true,
        delete: false,
        allow: true,
        children: [
          {
            name: "Pumpkins",
            read: true,
            write: false,
            edit: true,
            delete: false,
            allow: true
          },
          {
            name: "Carrots",
            read: true,
            write: false,
            edit: true,
            delete: true,
            allow: true
          }
        ]
      }
    ]
  }
];

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  
  name: string;
  read: boolean;
  write: boolean;
  edit: boolean;
  delete: boolean;
  allow: boolean;
  level: number;
}

/**
 * @title Tree with flat nodes
 */
@Component({
  selector: "tree-flat-overview-example",
  templateUrl: "tree-flat-overview-example.html",
  styleUrls: ["tree-flat-overview-example.css"]
})
export class TreeFlatOverviewExample {
  private _transformer = (node: FoodNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      read:node.read,
      write:node.write,
      edit:node.edit,
      delete:node.delete,
      allow:node.allow,
      level: level
    };
  };

  treeControl = new FlatTreeControl<ExampleFlatNode>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor() {
    this.dataSource.data = TREE_DATA;
  }

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;


  onReadPermissionChange(result:any) {
    console.log(result);
  }
}

/**  Copyright 2019 Google LLC. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */
